package net.therap.mealmanager.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 11/20/16
 */

@Entity
@Table(name = "item", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, length = 11)
    private int id;

    @Column(name = "name", length = 110, nullable = false)
    private String name;

    @Column(name = "cost", nullable = false)
    private int cost;

    public Item() {

    }

    public Item(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "meal", joinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "plan_id", referencedColumnName = "id")})
    private List<Plan> plans = new ArrayList<>(0);

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public void setPlans(List<Plan> plans) {
        this.plans = plans;
    }
}
