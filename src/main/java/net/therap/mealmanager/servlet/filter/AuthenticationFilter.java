package net.therap.mealmanager.servlet.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author al-amin
 * @since 11/28/16
 */

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        HttpSession session = req.getSession(false);

        System.out.println("Hello");

        if (session == null && !(uri.endsWith("home") || uri.endsWith("login") || uri.endsWith("signup")
                || uri.endsWith("logout") || uri.endsWith("meal") || uri.endsWith("item"))) {
            res.sendRedirect("home");
        } else {
            chain.doFilter(request, response);
        }
    }


    public void destroy() {

    }

}