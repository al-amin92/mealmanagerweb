package net.therap.mealmanager.view;

import net.therap.mealmanager.entity.Item;
import net.therap.mealmanager.entity.Plan;
import net.therap.mealmanager.object.DayTime;

import java.util.List;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class Show {

    private static final String format = "_________________________";

    public static void showHelp(boolean isAdmin) {

        System.out.printf("%-40s : %s\n", "help", "$ help");
        System.out.printf("%-40s : %s\n", "exit", "$ exit");
        System.out.printf("%-40s : %s\n", "show all food items", "$ show item");
        System.out.printf("%-40s : %s\n", "show a food item properties", "$ show item <item_id>");
        System.out.printf("%-40s : %s\n", "show meal items for a specific meal", "$ show meal sun lunch");
        System.out.printf("%-40s : %s\n", "show meal properties", "$ show properties");

        if (isAdmin) {
            System.out.printf("%-40s : %s\n", "set item 1,2 and 3 for tuesday lunch", "$ set meal tue lunch 1 2 3");
            System.out.printf("%-40s : %s\n", "insert item 4 and 6 for tuesday lunch", "$ insert meal tue lunch 4 6");
            System.out.printf("%-40s : %s\n", "remove item 2 and 5 for tuesday lunch", "$ delete meal tue lunch 2 5");
            System.out.printf("%-40s : %s\n", "insert an item to the item list",
                    "$ insert item <item_name> <item_cost>");
            System.out.printf("%-40s : %s\n", "delete an item from the item list", "$ delete item <item_id>");
            System.out.printf("%-40s : %s\n", "admin logout", "$ logout");
        } else {
            System.out.printf("%-40s : %s\n", "admin login", "$ login <username> <password>");
        }
    }

    public static void showItems(List<Item> itemList) {

        System.out.printf("%-25s | %-25s | %-25s\n", "item_id", "item_name", "item_cost");
        System.out.printf("%-25s_|_%-25s_|_%-25s\n", format, format, format);
        for (Item item : itemList) {
            System.out.printf("%-25s | %-25s | %-25s\n", item.getId(), item.getName(), item.getCost());
        }
        System.out.println();
    }

    public static void showMeal(List<Plan> planList) {

        DayTime dayTime = new DayTime();

        for (Plan plan : planList) {
            System.out.println(dayTime.getDayById(plan.getDay()) + " " + dayTime.getTimeById(plan.getTime())
                    + " (Per Meal Cost: " + plan.getCost() + ")");
            List<Item> itemList = plan.getItems();
            System.out.printf("%-25s | %-25s | %-25s\n", "item_id", "item_name", "item_cost");
            System.out.printf("%-25s_|_%-25s_|_%-25s\n", format, format, format);
            for (Item item : itemList) {
                System.out.printf("%-25s | %-25s | %-25s\n", item.getId(), item.getName(), item.getCost());
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void print(String printLine) {
        System.out.println(printLine);
    }
}
