package net.therap.mealmanager.servlet.session;

import net.therap.mealmanager.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @author al-amin
 * @since 11/28/16
 */
@WebServlet("/signup")
public class Signup extends HttpServlet {

    UserDao userDao = new UserDao();

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confermPassword = request.getParameter("passwordsignup_confirm");
        if (!password.equals(confermPassword)) {
            HttpSession session = request.getSession();
            session.setAttribute("logMsg", "please re-enter passwords");
            response.sendRedirect("/home#toregister");
        } else if (userDao.signup(username, password)) {
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            session.setAttribute("isAdmin", "true");
            session.setMaxInactiveInterval(30 * 60);
            Cookie userName = new Cookie("username", username);
            userName.setMaxAge(30 * 60);
            response.addCookie(userName);
            response.sendRedirect("/home#toregister");
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("logMsg", "username already taken, please try again.");
            response.sendRedirect("/home#toregister");
        }
    }

}
