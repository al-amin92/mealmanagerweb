package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.User;
import net.therap.mealmanager.utility.Util;
import net.therap.mealmanager.view.Show;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class UserDao {

    public static boolean admin = false, loggedIn = false;

    private boolean isAdmin = false;

    public UserDao() {

        SessionFactory sessionFactory2 = Util.getSessionAnnotationFactory();
        Session session2 = sessionFactory2.getCurrentSession();
        Transaction tx2 = session2.beginTransaction();

        Query query2 = session2.createQuery("FROM User WHERE username LIKE 'admin'");
        if (query2.uniqueResult() == null) {
            User user2 = new User();
            user2.setUsername("admin");
            user2.setPassword("therap");
            user2.setIsAdmin(true);
            session2.save(user2);
            session2.getTransaction().commit();
        } else {
            session2.close();
        }
    }

    public boolean login(String name, String password) {

        isAdmin = false;

        System.out.println(name + "-------" + password);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("FROM User WHERE username LIKE :name");
        query.setString("name", name);

        User user = (User) query.uniqueResult();

        if (user != null && user.getPassword().equals(password)) {
            loggedIn = true;
            if (user.getIsAdmin()) {
                admin = true;
                isAdmin = true;
                Show.print("Logged in as " + name + "(admin)");
            } else {
                Show.print("Logged in as " + name);
            }
            tx.commit();
            return true;
        } else {
            admin = loggedIn = false;
            Show.print("username and password doesn't match, try again.");
            tx.commit();
            return false;
        }
    }

    public void logout() {

        isAdmin = false;

        if (!loggedIn) {
            Show.print("You are not logged in.");
        } else {
            Show.print("Logged out.");
            admin = loggedIn = false;
            isAdmin = false;
        }
    }

    public boolean signup(String name, String password) {

        isAdmin = false;

        System.out.println(name + "-------" + password);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM User WHERE username LIKE :name");
        query.setString("name", name);
        if (query.uniqueResult() != null) {
            session.close();
            return false;
        }


        User user = new User();
        user.setUsername(name);
        user.setPassword(password);
        user.setIsAdmin(false);
        session.save(user);

        tx.commit();

        return true;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }
}
