<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]> <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]> <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->

<%--
  Created by IntelliJ IDEA.
  User: al-amin
  Date: 11/27/16
  Time: 11:41 AM
  To change this template use File | Settings | File Templates.
--%>


<html>
<head>
    <meta charset="UTF-8"/>
    <title>Therap Meal Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3"/>
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class"/>
    <meta name="author" content="Codrops"/>
    <link rel="shortcut icon" href="../favicon.ico">
    <style type="text/css">
        <%@include file="../css/demo.css"%>
        <%@include file="../css/style.css"%>
        <%@include file="../css/animate-custom.css"%>
    </style>
</head>
<body>
<div class="container">

    <header>
        <h1>Therap <span>Meal Manager</span></h1>
        <nav class="codrops-demos">
            <a href="home" class="current-demo">Home</a>
            <a href="item">Available Items</a>
            <a href="meal">Meal Plans</a>
            <c:if test="${username eq null}">
                <a href="#tologin">Login</a>
                <a href="#toregister">Signup</a>
            </c:if>
            <c:if test="${username ne null}">
                <span>Logged in as <strong><c:out value="${username}"/></strong></span>
            <span><form action="logout" method="POST">
                <input type="submit" value="Logout"/>
            </form></span>
            </c:if>
        </nav>
    </header>

    <section>
        <div id="container_demo">

            <c:if test="${username eq null}">


                <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4 -->
                <a class="hiddenanchor" id="toregister"></a>
                <a class="hiddenanchor" id="tologin"></a>

                <div id="wrapper">

                    <div id="login" class="animate form">
                        <form action="login" method="POST">
                            <c:if test="${logMsg ne null}">
                                <c:out value="${logMsg}"/>

                            </c:if>
                            <h1>Log in</h1>

                            <p>
                                <label for="username" class="uname" data-icon="u"> Your username </label>
                                <input id="username" name="username" required="required" type="text"
                                       placeholder="eg. myusername"/>
                            </p>

                            <p>
                                <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                <input id="password" name="password" required="required" type="password"
                                       placeholder="eg. X8df!90EO"/>
                            </p>

                            <p class="login button">
                                <input type="submit" value="Login"/>
                            </p>

                            <p class="change_link">
                                Not a member yet ?
                                <a href="#toregister" class="to_register">Join us</a>
                            </p>
                        </form>
                    </div>

                    <div id="register" class="animate form">
                        <form action="signup" method="POST">
                            <c:if test="${logMsg ne null}">
                                <c:out value="${logMsg}"/>

                            </c:if>
                            <h1> Sign up </h1>

                            <p>
                                <label for="username" class="uname" data-icon="u"> Your username </label>
                                <input id="username" name="username" required="required" type="text"
                                       placeholder="eg. myusername"/>
                            </p>

                            <p>
                                <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                <input id="password" name="password" required="required" type="password"
                                       placeholder="eg. X8df!90EO"/>
                            </p>

                            <p>
                                <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your
                                    password </label>
                                <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required"
                                       type="password" placeholder="eg. X8df!90EO"/>
                            </p>

                            <p class="signin button">
                                <input type="submit" value="Sign up"/>
                            </p>

                            <p class="change_link">
                                Already a member ?
                                <a href="#tologin" class="to_register"> Go and log in </a>
                            </p>
                        </form>
                    </div>
                    <c:set var="logMsg" scope="session" value="${null}"/>
                </div>

            </c:if>
            <c:if test="${username ne null}">
                <div id="wrapper">
                    <div id="register2" class="animate form">
                        <form action="item" method="POST">
                            <h1> Items </h1>

                            <p>
                                <label for="username" class="uname"> Search Items </label>
                                <input id="username" name="search" type="text" placeholder="eg. apple,12,13,banana"/>
                            </p>

                            <p>
                                <label for="password" class="youpasswd"> Criteria </label>
                                <select name="criteria">
                                    <option value="any">Any</option>
                                    <option value="id">By Id</option>
                                    <option value="name">By Name</option>
                                </select>
                            </p>

                            <p class="signin button">
                                <input type="submit" value="Show Items"/>
                            </p>
                        </form>
                    </div>

                    <div id="register2" class="animate form">
                        <form action="meal" method="POST">
                            <h1> Meal Plans </h1>

                            <p>
                                <label for="password" class="youpasswd">Day of Meal</label>
                                <select name="day">
                                    <option value="all">All</option>
                                    <option value="sun">Sunday</option>
                                    <option value="mon">Monday</option>
                                    <option value="tue">Tuesday</option>
                                    <option value="wed">Wednesday</option>
                                    <option value="thu">Thursday</option>
                                    <option value="fri">Friday</option>
                                    <option value="sat">Saturday</option>
                                </select>
                            </p>
                            <p>
                                <label for="passwordsignup_confirm" class="youpasswd">Time of Meal</label>
                                <select name="time">
                                    <option value="all">All</option>
                                    <option value="breakfast">Breakfast</option>
                                    <option value="lunch">Lunch</option>
                                    <option value="dinner">dinner</option>
                                </select>
                            </p>
                            <p class="signin button">
                                <input type="submit" value="Show Meals"/>
                            </p>
                        </form>
                    </div>

                </div>
            </c:if>
        </div>
    </section>
</div>
</body>
</html>
