<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: al-amin
  Date: 11/27/16
  Time: 12:07 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>Therap Meal Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3"/>
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class"/>
    <meta name="author" content="Codrops"/>
    <link rel="shortcut icon" href="../favicon.ico">
    <style type="text/css">
        <%@include file="../css/demo.css"%>
        <%@include file="../css/style.css"%>
        <%@include file="../css/animate-custom.css"%>
        <%@include file="../css/tablebutton.css"%>
    </style>
</head>
<body>
<div class="container">
    <header>
        <h1>Therap <span>Meal Manager</span></h1>
        <nav class="codrops-demos">
            <a href="home">Home</a>
            <a href="item">Available Items</a>
            <a href="meal" class="current-demo">Meal Plans</a>
            <c:if test="${username eq null}">
                <a href="home#tologin">Login</a>
                <a href="home#toregister">Signup</a>
            </c:if>
            <c:if test="${username ne null}">
                <span>Logged in as <strong><c:out value="${username}"/></strong></span>
                <span><form action="logout" method="POST">
                    <button class="button4" type="submit" value="Logout">Logout</button>
                </form></span>
            </c:if>
        </nav>
    </header>
    <div>
        <section>
            <div id="container_demo">

                <div id="wrapper">
                    <h1><span>Items</span></h1>

                    <table>
                        <jsp:useBean id="insertitembean" class="net.therap.mealmanager.bean.InsertItemBean"
                                     scope="session">
                            <jsp:setProperty name="insertitembean" property="id" value="0"/>
                            <jsp:setProperty name="insertitembean" property="itemList" value="${null}"/>
                        </jsp:useBean>
                        <c:forEach items="${mealList}" var="meal">
                            <tr>
                                <jsp:useBean id="daytimebean" class="net.therap.mealmanager.bean.DayTimeBean"
                                             scope="session">
                                    <jsp:setProperty name="daytimebean" property="dId" value="${meal.day}"/>
                                    <jsp:setProperty name="daytimebean" property="tId" value="${meal.time}"/>
                                    <jsp:setProperty name="daytimebean" property="day" value=""/>
                                    <jsp:setProperty name="daytimebean" property="time" value=""/>
                                </jsp:useBean>
                                <th>
                                    <c:out value="${meal.id}"/>
                                </th>
                                <th>
                                    <c:out value="${daytimebean.day}"/>
                                </th>
                                <th>
                                    <c:out value="${daytimebean.time}"/>
                                </th>
                                <th>
                                    <c:out value="${meal.cost}"/>
                                </th>
                                <c:if test="${isAdmin ne null}">
                                    <th>
                                        <form action="meal" method="POST">
                                            <select name="insert">
                                                <c:forEach items="${insertitembean.itemList}" var="item">
                                                    <option value=<c:out value="${item.id}"/>>
                                                        <c:out value="${item.name} (${item.cost} taka.)"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                            <input type="hidden" name="insertDay" value=<c:out
                                                    value="${daytimebean.day}"/>>
                                            <input type="hidden" name="insertTime" value=<c:out
                                                    value="${daytimebean.time}"/>>
                                            <input type="hidden" name="day" value=<c:out value="all"/>>
                                            <input type="hidden" name="time" value=<c:out value="all"/>>
                                            <button class="button4" type="submit">Insert Item</button>
                                        </form>
                                    </th>
                                </c:if>
                            </tr>

                            <jsp:useBean id="itembean" class="net.therap.mealmanager.bean.ItemBean" scope="session">
                                <jsp:setProperty name="itembean" property="id" value="${meal.id}"/>
                                <jsp:setProperty name="itembean" property="itemList" value="${meal.items}"/>
                            </jsp:useBean>

                            <c:forEach items="${itembean.itemList}" var="item">
                                <tr>
                                    <td></td>
                                    <td>
                                        <c:out value="${item.id}"/>
                                    </td>
                                    <td>
                                        <c:out value="${item.name}"/>
                                    </td>
                                    <td>
                                        <c:out value="${item.cost}"/>
                                    </td>
                                    <c:if test="${isAdmin ne null}">
                                        <td>
                                            <form action="meal" method="POST">
                                                <input type="hidden" name="delete" value=<c:out value="${item.id}"/>>
                                                <input type="hidden" name="deleteDay" value=<c:out
                                                        value="${daytimebean.day}"/>>
                                                <input type="hidden" name="deleteTime" value=<c:out
                                                        value="${daytimebean.time}"/>>
                                                <input type="hidden" name="day" value=<c:out value="all"/>>
                                                <input type="hidden" name="time" value=<c:out value="all"/>>
                                                <button class="button3" type="submit">Delete Item</button>
                                            </form>
                                        </td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td>_____</td>
                                <td>_______________</td>
                                <td>_______________</td>
                                <td>____________</td>
                                <c:if test="${isAdmin ne null}">
                                    <td>_____________________</td>
                                </c:if></tr>
                            <c:remove var="itembean" scope="session"/>
                            <c:remove var="daytimebean" scope="session"/>
                        </c:forEach>
                        <c:remove var="insertitembean" scope="session"/>
                    </table>
                    <br><br><br><br>
                </div>
            </div>
        </section>
    </div>
</div>
</body>
</html>
