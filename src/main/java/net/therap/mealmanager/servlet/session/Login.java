package net.therap.mealmanager.servlet.session;

/**
 * @author al-amin
 * @since 11/28/16
 */

import net.therap.mealmanager.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class Login extends HttpServlet {

    UserDao userDao = new UserDao();

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (userDao.login(username, password)) {
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            if (userDao.getIsAdmin()) {
                session.setAttribute("isAdmin", "true");
            }
            session.setMaxInactiveInterval(30 * 60);
            Cookie userName = new Cookie("username", username);
            userName.setMaxAge(30 * 60);
            response.addCookie(userName);
            response.sendRedirect("/home");
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("logMsg", "usernamr or password incorrect, try again.");
            response.sendRedirect("/home");
        }
    }

}