package net.therap.mealmanager.servlet.session;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.entity.Item;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 11/29/16
 */
@WebServlet(urlPatterns = {"/item"})
public class Items extends HttpServlet {
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        ItemDao itemDao = new ItemDao();
        List<Item> itemList = new ArrayList<>();
        if (session.getAttribute("isAdmin") != null && session.getAttribute("isAdmin").equals("true")) {
            if (request.getParameter("insert") != null && request.getParameter("name") != null
                    && request.getParameter("cost") != null) {
                request.setAttribute("insert", null);
                itemDao.insert(request.getParameter("name"), request.getParameter("cost"));
            }
            if (request.getParameter("delete") != null) {
                request.setAttribute("delete", null);
                itemDao.delete(request.getParameter("delete").toString());
            }
            if (request.getParameter("update") != null && request.getParameter("name") != null
                    && request.getParameter("cost") != null) {
                itemDao.update(request.getParameter("update").toString(), request.getParameter("name")
                        , request.getParameter("cost"));
                request.setAttribute("update", null);
            }
        }

        String search = null;
        String criteria = "any";

        if (request.getParameter("search") != null && request.getParameter("search") != "") {
            search = request.getParameter("search");
        }
        if (request.getParameter("criteria") != null && request.getParameter("criteria") != "") {
            criteria = request.getParameter("criteria");
        }


        if (search != null && search != "") {
            if (criteria.equals("id")) {
                itemList.add(itemDao.queryId(search));
            } else if (criteria.equals("name")) {
                itemList = itemDao.queryName(search);
            } else {
                try {
                    Integer.parseInt(search);
                    itemList.add(itemDao.queryId(search));
                } catch (Exception e) {
                    itemList = itemDao.queryName(search);
                }

            }
        } else {
            itemList = itemDao.query();
        }

        List<Item> itemListUpdate = itemDao.query();

        session.setAttribute("itemList", itemList);
        session.setAttribute("itemListUpdate", itemListUpdate);

        request.getRequestDispatcher("/WEB-INF/JSP/item.jsp").forward(request, response);
    }
}
