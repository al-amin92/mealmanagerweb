package net.therap.mealmanager.bean;

import net.therap.mealmanager.dao.PlanDao;
import net.therap.mealmanager.entity.Item;

import java.util.List;

/**
 * @author al-amin
 * @since 11/30/16
 */
public class ItemBean {

    private int id;

    private List<Item> itemList, itemList2;

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        PlanDao planDao = new PlanDao();
        List<Item> itemList1 = planDao.query(id);
        this.itemList2 = itemList1;
        this.id = id;
    }
}
