package net.therap.mealmanager.servlet.session;

import net.therap.mealmanager.dao.PlanDao;
import net.therap.mealmanager.entity.Plan;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author al-amin
 * @since 11/29/16
 */
@WebServlet(urlPatterns = {"/meal"})
public class Meals extends HttpServlet {
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        PlanDao planDao = new PlanDao();

        if (session.getAttribute("isAdmin") != null && session.getAttribute("isAdmin").equals("true")) {
            if (request.getParameter("insert") != null && request.getParameter("insertDay") != null
                    && request.getParameter("insertTime") != null) {
                planDao.insert(request.getParameter("insertDay"),
                        request.getParameter("insertTime"), request.getParameter("insert"));
                request.setAttribute("insert", null);
            }
            if (request.getParameter("delete") != null && request.getParameter("deleteDay") != null
                    && request.getParameter("deleteTime") != null) {
                planDao.delete(request.getParameter("deleteDay"),
                        request.getParameter("deleteTime"), request.getParameter("delete"));
                request.setAttribute("delete", null);
            }
        }

        List<Plan> mealList;

        String day = "all";
        String time = "all";

        if (request.getParameter("day") != null && request.getParameter("day") != "") {
            day = request.getParameter("day");
        }

        if (request.getParameter("time") != null && request.getParameter("time") != "") {
            time = request.getParameter("time");
        }

        if (day.equals("all") && time.equals("all")) {
            mealList = planDao.query();
        } else if (day.equals("all")) {
            mealList = planDao.queryTime(time);
        } else if (time.equals("all")) {
            mealList = planDao.queryDay(day);
        } else {
            mealList = planDao.query(day, time);
        }

        session.setAttribute("mealList", mealList);

        request.getRequestDispatcher("/WEB-INF/JSP/meal.jsp").forward(request, response);
    }
}
