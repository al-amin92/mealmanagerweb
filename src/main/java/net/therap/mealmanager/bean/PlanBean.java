package net.therap.mealmanager.bean;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.entity.Plan;

import java.util.List;

/**
 * @author al-amin
 * @since 11/29/16
 */
public class PlanBean {

    private int id;

    private List<Plan> planList, planList2;

    public List<Plan> getPlanList() {
        return planList;
    }

    public void setPlanList(List<Plan> planList) {
        this.planList = planList2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        ItemDao itemDao = new ItemDao();
        List<Plan> planList1 = itemDao.query(Integer.toString(id));
        this.planList2 = planList1;
        this.id = id;
    }

}
