package net.therap.mealmanager.bean;

import net.therap.mealmanager.object.DayTime;

/**
 * @author al-amin
 * @since 11/30/16
 */
public class DayTimeBean {

    private int dId;
    private int tId;
    private String day;
    private String time;

    public int getdId() {
        return dId;
    }

    public void setdId(int dId) {
        DayTime dayTime = new DayTime();
        this.day = dayTime.getDayById(dId);
        this.dId = dId;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        DayTime dayTime = new DayTime();
        this.time = dayTime.getTimeById(tId);
        this.tId = tId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day += day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time += time;
    }
}
