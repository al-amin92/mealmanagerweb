package net.therap.mealmanager.queryprocessor;

import java.util.Scanner;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class Input {
    public void takeInput() {

        String command;

        QueryProcessor queryProcessor = new QueryProcessor();
        queryProcessor.process("help");

        Scanner sc = new Scanner(System.in);

        while ((command = sc.nextLine()) != null) {
            if (!queryProcessor.process(command)) {
                System.out.println("Closing Database...");
                break;
            }
        }
    }
}
