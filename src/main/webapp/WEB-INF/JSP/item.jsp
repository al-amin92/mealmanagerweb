<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: al-amin
  Date: 11/27/16
  Time: 2:53 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>Therap Meal Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3"/>
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class"/>
    <meta name="author" content="Codrops"/>
    <link rel="shortcut icon" href="../favicon.ico">
    <style type="text/css">
        <%@include file="../css/demo.css"%>
        <%@include file="../css/style.css"%>
        <%@include file="../css/animate-custom.css"%>
        <%@include file="../css/tablebutton.css"%>
    </style>
</head>
<body>
<div class="container">
    <header>
        <h1>Therap <span>Meal Manager</span></h1>
        <nav class="codrops-demos">
            <a href="home">Home</a>
            <a href="item" class="current-demo">Available Items</a>
            <a href="meal">Meal Plans</a>
            <c:if test="${username eq null}">
                <a href="home#tologin">Login</a>
                <a href="home#toregister">Signup</a>
            </c:if>
            <c:if test="${username ne null}">
                <span>Logged in as <strong><c:out value="${username}"/></strong></span>
                <span><form action="logout" method="POST">
                    <button class="button4" type="submit" value="Logout">Logout</button>
                </form></span>
            </c:if>
        </nav>
    </header>
    <div>
        <section>
            <div id="container_demo">

                <div id="wrapper">
                    <h1><span>Items</span></h1>
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Meals</th>
                            <c:if test="${isAdmin ne null}">
                                <th>Action</th>
                            </c:if>
                        </tr>
                        <c:forEach items="${itemList}" var="item">
                            <tr>
                                <td>
                                    <c:out value="${item.id}"/>
                                </td>
                                <td>
                                    <c:out value="${item.name}"/>
                                </td>
                                <td>
                                    <c:out value="${item.cost}"/>
                                </td>

                                <td>
                                    <jsp:useBean id="planbean" class="net.therap.mealmanager.bean.PlanBean"
                                                 scope="session">
                                        <jsp:setProperty name="planbean" property="id" value="${item.id}"/>
                                        <jsp:setProperty name="planbean" property="planList" value="${item.plans}"/>
                                    </jsp:useBean>
                                    <c:forEach items="${planbean.planList}" var="plan">

                                        <jsp:useBean id="daytimebean" class="net.therap.mealmanager.bean.DayTimeBean"
                                                     scope="session">
                                            <jsp:setProperty name="daytimebean" property="dId" value="${plan.day}"/>
                                            <jsp:setProperty name="daytimebean" property="tId" value="${plan.time}"/>
                                            <jsp:setProperty name="daytimebean" property="day" value=""/>
                                            <jsp:setProperty name="daytimebean" property="time" value=""/>
                                        </jsp:useBean>
                                        <c:out value="${daytimebean.day}-${daytimebean.time}"/>
                                        <c:remove var="daytimebean" scope="session"/>
                                        <br>
                                    </c:forEach>
                                    <c:remove var="planbean" scope="session"/>
                                </td>

                                <c:if test="${isAdmin ne null}">
                                    <td>
                                        <form action="item" method="POST">
                                            <input type="hidden" name="delete" value=<c:out value="${item.id}"/>>
                                            <button class="button3" type="submit">Delete Item</button>
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>

                    <c:if test="${isAdmin ne null}">
                        <br><br>

                        <h1>Insert</h1>

                        <div>
                            <form action="item" method="POST">
                                <row>
                                    <input type="hidden" name="insert" value="true">
                                    <input name="name" type="text" placeholder="item name" required>
                                    <input name="cost" type="number" placeholder="item cost" required>
                                    <button type="submit" class="button">Insert New Item</button>
                                </row>
                            </form>
                        </div>
                        <br><br>

                        <h1>Update</h1>

                        <div>
                            <form action="item" method="POST">
                                <row>
                                    <select name="update">
                                        <c:forEach items="${itemListUpdate}" var="item">
                                        <option value=
                                            <c:out value="${item.id}"/>>
                                                <c:out value="${item.name} (${item.cost}taka)"/>
                                            </c:forEach>
                                    </select>
                                    <input name="name" type="text" placeholder="new name">
                                    <input name="cost" type="number" placeholder="new cost">
                                    <button type="submit" class="button2">Update Item</button>
                                </row>
                            </form>
                        </div>
                        <br><br><br>
                    </c:if>
                </div>

            </div>
        </section>
    </div>
</div>
</body>
</html>