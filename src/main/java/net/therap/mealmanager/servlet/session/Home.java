package net.therap.mealmanager.servlet.session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author al-amin
 * @since 11/29/16
 */

@WebServlet(urlPatterns = {"/", "/home"})
public class Home extends HttpServlet {

    private HttpSession session;

    public void setSession(HttpSession session) {
        this.session = session;
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession());
        request.getRequestDispatcher("/WEB-INF/JSP/home.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession());
        request.getRequestDispatcher("/WEB-INF/JSP/home.jsp").forward(request, response);
    }


}
